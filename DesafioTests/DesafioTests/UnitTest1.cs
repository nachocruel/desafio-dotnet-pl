using desafio_dotnet_pl.Models;
using NUnit.Framework;
using System;
using System.IO;

namespace DesafioTests
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            Console.WriteLine("Setup call");
            if(!Directory.Exists("Assets"))
            {
                Console.WriteLine(TestContext.CurrentContext.TestDirectory + @"Assets\TodosPaises.txt");
                string text = File.ReadAllText(TestContext.CurrentContext.TestDirectory + @"Assets\TodosPaises.txt");
                Directory.CreateDirectory("Assets");
                File.WriteAllText(@"Assets\TodosPaises.txt", text, System.Text.Encoding.UTF8);
            }
        }

        [Test]
        public void testaCadastroAutor()
        {
           /* // Teste Cadastro normal
            Autor autor = new Autor()
            {
                Nome = "Jorge Amado",
                Cpf = "22802503065",
                Email = "jorgeamado@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Mexico",
                Sexo = "Masculino"
            };
            Assert.That(desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor), Is.True);*/



            // Teste para CPF Invalido
            Autor autor2 = new Autor()
            {
                Nome = "Jo�o Campos",
                Cpf = "88547758475",
                Email = "joao@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Bolivia",
                Sexo = "Masculino"
            };
            var ex = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor2));
            Assert.That(ex.Message, Is.EqualTo("O CPF informado � inv�lido!"));

            
            // Teste Email igual inv�lido
            Autor autor3 = new Autor()
            {
                Nome = "Maria Julia",
                Cpf = "77129881020",
                Email = "maria@desafio",
                DataDeNascimento = new System.DateTime(),
                Pais = "Bolivia",
                Sexo = "Feminino"
            };
            var ex2 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor3));
            Assert.That(ex2.Message, Is.EqualTo("O email informado � inv�lido!"));

            
            // Pais Brazil CPF n�o informado
            Autor autor4 = new Autor()
            {
                Nome = "Maria Olimpia",
                Cpf = "",
                Email = "mariaolimpia@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Brazil",
                Sexo = "Masculino"
            };
            var ex3 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor4));
            Assert.That(ex3.Message, Is.EqualTo("O CPF � obrigat�rio para autores brasileiros!"));

            
            // Teste de nome nulo
            Autor autor5 = new Autor()
            {
                Nome = null,
                Cpf = "82925770096",
                Email = "joao@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Australia",
                Sexo = "Masculino"
            };
            var ex4 = Assert.Throws<ArgumentException>(() => { desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor5); });
            Assert.That(ex4.Message, Is.EqualTo("O nome do autor � um campo obrigat�rio!"));


            
            // Teste de Pais invalido
            Autor autor6 = new Autor()
            {
                Nome = "Julio Campos",
                Cpf = "79473924046",
                Email = "marcos@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Terra m�dia do sul",
                Sexo = "Masculino"
            };
            var ex5 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor6));
            Assert.That(ex5.Message, Is.EqualTo("O pa�s informado n�o � v�lido!"));
            

            //Teste email j� existe
            Autor autor7 = new Autor()
            {
                Nome = "Jo�o Campos",
                Cpf = "81208549006",
                Email = "jorgeamado@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Brazil",
                Sexo = "Masculino"
            };
           var ex6 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.AutorProcessor.cadastrarAutor(autor7));
            Assert.That(ex6.Message, Is.EqualTo("J� existe um cadastro para o email informado!"));
        }

        
        
        [Test]
        public void testCadastroObra()
        {
            Obra obra = new Obra()
            {
                Nome = "Dom Casmurro",
                LinkImagem = "",
                DataDeExposicao = null,
                DataDePublicacao = new System.DateTime(1842, 2, 3, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                Descricao = "Historia da fase realista do bruxo do cosme velho",
            };

            Autor autor = new Autor()
            {
                Id = 3,
                Nome = "Edvan",
                Cpf = "00694766160",
                Email = "edvan@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Brazil",
                Sexo = "Masculino",
                TabAutorObra = {}
            };
            Assert.AreEqual(true, desafio_dotnet_pl.Processor.ObraProcessor.cadastrarObra(obra, autor));
            

            // Data de publica��o e exposi��o n�o infomadas
            Obra obra2 = new Obra()
            {
                Nome = "Dom Casmurro",
                LinkImagem = "",
                DataDeExposicao = null,
                DataDePublicacao = null,
                Descricao = "Historia da fase realista do bruxo do cosme velho",
            };
            var ex = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.ObraProcessor.cadastrarObra(obra2, autor));
            Assert.That(ex.Message, Is.EqualTo("N�o foi informado a data de publica��o ou a de exposi��o da obra!"));


            // Nome do autor n�o informado
            Obra obra3 = new Obra()
            {
                Nome = "",
                LinkImagem = "",
                DataDeExposicao = new DateTime(),
                DataDePublicacao = null,
                Descricao = "Historia da fase realista do bruxo do cosme velho",
            };
            var ex3 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.ObraProcessor.cadastrarObra(obra3, autor));
            Assert.That(ex3.Message, Is.EqualTo("O nome da obra � um campo obrigat�rio!"));


            // Descri��o da obra n�o informado
            Obra obra4 = new Obra()
            {
                Nome = "Manuel Bandeira",
                LinkImagem = "",
                DataDeExposicao = new DateTime(),
                DataDePublicacao = null,
                Descricao = "",
            };
            var ex4 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.ObraProcessor.cadastrarObra(obra4, autor));
            Assert.That(ex4.Message, Is.EqualTo("A descri��o da obra � um campo obrigat�rio!"));


            // Descri��o com mais de 240 caracteres.
            Obra obra5 = new Obra()
            {
                Nome = "Manuel Bandeira",
                LinkImagem = "",
                DataDeExposicao = new DateTime(),
                DataDePublicacao = null,
                Descricao = "lakdjfioajdoijfiojaiosjdiofjoijaoisdjfoijioajiosdjoifajdiojfiojaoijdiofjioasiodjfoijaiosjdiofjioajsdiofjioajsdoijfiojaoijsdiofjioajiodsfjiojaiodjfoijaiosjdfiojasdjioaosjdiofjioajsdiojfiajsdiojfioajiosdjfiajiodsjfiojaiojdsoifjoijasoijdofijaijasdjfoijasjdfiojiojfioajsdiofjiajsdiofjoijasoijdfjaiosjdfjaiosjdiofjioajsiodfjioajsdiofjasiodjfoijaisdfi",
            };
            var ex5 = Assert.Throws<ArgumentException>(() => desafio_dotnet_pl.Processor.ObraProcessor.cadastrarObra(obra5, autor));
            Assert.That(ex5.Message, Is.EqualTo("A descri��o da obra n�o pode conter no m�ximo 240 caracteres!"));
        }

        [Test]
        public void testDeleteObra()
        {
            Obra obra = new Obra()
            {
                Nome = "Dom Casmurro",
                LinkImagem = "",
                DataDeExposicao = null,
                DataDePublicacao = new System.DateTime(1842, 2, 3, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                Descricao = "Historia da fase realista do bruxo do cosme velho",
            };
            Assert.That(desafio_dotnet_pl.Processor.ObraProcessor.removeObra(obra), Is.True);
        }

        [Test]
        public void testRemoveAutor()
        {

            Autor autor = new Autor()
            {
                Id = 0,
                Nome = "Machado de Assis",
                Cpf = "01939676002",
                Email = "machadoassis@desafio.com",
                DataDeNascimento = new System.DateTime(),
                Pais = "Brazil",
                Sexo = "Masculino",
                TabAutorObra = { }
            };

            var ex2 = Assert.Throws<ArgumentException>(() => { desafio_dotnet_pl.Processor.AutorProcessor.deleteAutor(autor); });
            Assert.That(ex2.Message, Is.EqualTo("O autor n�o pode ser removido, porque ele possui obras vinculadas!"));
        }

    }
}