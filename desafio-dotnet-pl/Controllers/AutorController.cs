﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using desafio_dotnet_pl.Helpers;
using desafio_dotnet_pl.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace desafio_dotnet_pl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutorController : ControllerBase
    {
        public AutorController()
        {
        }

        [HttpGet]
        public IEnumerable<Autor> GetAutores(int min, int max)
        {
            try
            {
                return DAO.AutorDAO.getAutores(min, max);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpGet("v1")]
        public IEnumerable<Autor> GetAutorObra(long id_obra)
        {
            try
            {
                return DAO.AutorDAO.getAutoresObra(id_obra);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("v2")]
        public IEnumerable<Autor> getAutorByName(string parcial)
        {
            try
            {
                return DAO.AutorDAO.getAutorByName(parcial);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public bool cadastrarAutor(Autor autor)
        {
            try
            {
                return Processor.AutorProcessor.cadastrarAutor(autor);
            }
            catch (ArgumentException ae)
            {
                throw ae;
            }
        }

        [HttpDelete]
        public bool deleteAutor(Autor autor)
        {
            try
            {
                return Processor.AutorProcessor.deleteAutor(autor);
            }
            catch (ArgumentException ae)
            {
                throw ae;
            }
        }

        [HttpPut]
        public bool atualizarAutor(Autor autor)
        {
            try
            {
                return DAO.AutorDAO.updateAutor(autor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}