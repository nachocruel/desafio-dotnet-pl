﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using desafio_dotnet_pl.Exceptions;
using desafio_dotnet_pl.Helpers;
using desafio_dotnet_pl.Models;
using desafio_dotnet_pl.Processor;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace desafio_dotnet_pl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ObraController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Obra> Get(int min, int max)
        {
            try
            {
                return DAO.ObraDAO.getObras(min, max);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public bool cadastrarObra(long id_autor, Obra obra)
        {
            try
            {
                Autor autor = DAO.AutorDAO.getAutorFromId(id_autor);
                return ObraProcessor.cadastrarObra(obra, autor);
            }
            catch (ArgumentException ae)
            {
                LogWriter.writeLog(ae.Message);
                throw ae;
            }
            catch (NullReferenceException nfe)
            {
                throw nfe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        public bool removeObra(Obra obra)
        {
            try
            {
                return Processor.ObraProcessor.removeObra(obra);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}