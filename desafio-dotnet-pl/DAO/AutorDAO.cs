﻿using desafio_dotnet_pl.Helpers;
using desafio_dotnet_pl.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.DAO
{
    public class AutorDAO
    {
        public static bool addAutor(Autor autor)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    context.Autor.Add(autor);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static bool removeAutor(Autor autor)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    Autor autoRemove = context.Autor
                        .Where(a => a.Id == autor.Id || a.Cpf == autor.Cpf).FirstOrDefault<Autor>();

                    if(autoRemove != null)
                    {
                        context.Autor.Attach(autoRemove);
                        context.Autor.Remove(autoRemove);
                        context.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static Autor getAutorFormCpf(string cpf)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return context.Autor.Where(a => a.Cpf == cpf).FirstOrDefault<Autor>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static Autor getAutorFromId(long id)
        {
            try
            {
                using(var context = new desafiodbContext())
                {
                    Autor autor = context.Autor.Where(a => a.Id == id).FirstOrDefault<Autor>();
                    if (autor != null)
                        return autor;
                    else
                        throw new System.NullReferenceException("O autor informado não foi encontrado");
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message);
                throw;
            }
        }
        public static Autor getAutorFromEmail(string email)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return context.Autor.Where(a => a.Email == email).FirstOrDefault<Autor>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static List<Autor> getAutorByName(string name)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return (from a in context.Autor where a.Nome.Contains(name) select a).Take(10).ToList<Autor>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message);
                throw;
            }
        }

        public static List<Autor> getAutores(int min, int max)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return context.Autor.Skip(min).Take(max).ToList<Autor>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static bool updateAutor(Autor autor)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    Autor autorToUpdate = context.Autor.Where(a => a.Id == autor.Id).FirstOrDefault<Autor>();
                    if (autorToUpdate != null)
                    {
                        autorToUpdate.Nome = autor.Nome;
                        autorToUpdate.Email = autor.Email;
                        autorToUpdate.Cpf = autor.Cpf;
                        autorToUpdate.DataDeNascimento = autor.DataDeNascimento;
                        autorToUpdate.Pais = autor.Pais;
                        autorToUpdate.TabAutorObra = autor.TabAutorObra;
                        context.Entry(autorToUpdate).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        context.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static List<Autor> getAutoresObra(long id_obra)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return (from autor_obra in context.TabAutorObra
                            join autor in context.Autor on autor_obra.IdObra equals id_obra
                            where (autor.Id == autor_obra.IdAutor)
                            select autor).ToList<Autor>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }
    }
}
