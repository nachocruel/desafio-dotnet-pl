﻿using desafio_dotnet_pl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.DAO
{
    public class ConnectionDAO
    {
        
        public static bool DataBaseConnected()
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return context.Database.CanConnect();
                }
            }
            catch (Exception ex)
            {
                Helpers.LogWriter.writeLog(ex.Message);
                return false;
            }
        }
    }
}
