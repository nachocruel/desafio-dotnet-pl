﻿using desafio_dotnet_pl.Helpers;
using desafio_dotnet_pl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.DAO
{
    public class ObraDAO
    {
        public static bool addObra(Obra obra, Autor autor)
        {
            using (var context = new desafiodbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Obra.Add(obra);
                        context.SaveChanges();

                        TabAutorObra obraAutor = new TabAutorObra();
                        obraAutor.IdAutor = autor.Id;
                        obraAutor.IdObra = obra.Id;
                        context.TabAutorObra.Add(obraAutor);
                        context.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        LogWriter.writeLog(ex.Message);
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public static bool removeObra(Obra obra)
        {
            using (var context = new desafiodbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        List<TabAutorObra> autorObras = context.TabAutorObra.Where(ao => ao.IdObra == obra.Id).ToList<TabAutorObra>();
                        autorObras.ForEach(ao =>
                        {
                            context.TabAutorObra.Attach(ao);
                            context.TabAutorObra.Remove(ao);
                            context.SaveChanges();
                        });
                        context.Obra.Attach(obra);
                        context.Obra.Remove(obra);
                        context.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        LogWriter.writeLog(ex.Message);
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public static List<Obra> getObras(int min, int max)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return context.Obra.Skip(min).Take(max).ToList<Obra>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static bool updateAutor(Obra obra)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    Obra obraUpadated = context.Obra.Where(o => o.Id == obra.Id).FirstOrDefault<Obra>();
                    obraUpadated.DataDeExposicao = obra.DataDeExposicao;
                    obraUpadated.DataDePublicacao = obra.DataDePublicacao;
                    obraUpadated.LinkImagem = obra.LinkImagem;
                    obraUpadated.Nome = obra.Nome;
                    context.Entry(obraUpadated).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public static List<Obra> getObrasAutor(long id_autor)
        {
            try
            {
                using (var context = new desafiodbContext())
                {
                    return (from autor_obra in context.TabAutorObra
                            join obra in context.Obra on autor_obra.IdAutor equals id_autor
                            where (obra.Id == autor_obra.IdObra)
                            select obra).ToList<Obra>();
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw;
            }
        }
    }
}

