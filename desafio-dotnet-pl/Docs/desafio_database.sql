
CREATE DATABASE IF NOT EXISTS desafiodb;

create table autor (
    id bigint(20) NOT NULL auto_increment,
    nome varchar(120) NOT NULL,
    email varchar(100) UNIQUE,
    data_de_nascimento DATETIME NOT NULL,
    pais varchar(100) NOT NULL,
    cpf varchar(11) UNIQUE,
    sexo varchar(15) default null,
    PRIMARY KEY (id),
    CHECK(NOT((pais='Brasil' or pais='BRAZIL') AND cpf is null))
);


create table obra(
    id bigint(20) NOT NULL auto_increment,
    nome varchar(120) NOT NULL,
    link_imagem varchar(320) not null,
    data_de_publicacao DATETIME,
    data_de_exposicao DATETIME,
    descricao varchar(240) NOT NULL,
    PRIMARY KEY(id),
    CHECK (NOT((data_de_publicacao is not null and data_de_exposicao is not null) or (data_de_publicacao is null and data_de_exposicao is null)))
);


create table tab_autor_obra (
 id_autor bigint(20) NOT NULL,
 id_obra bigint (20) NOT NULL,
 PRIMARY KEY (id_autor, id_obra),
 FOREIGN KEY (id_autor) REFERENCES autor (id),
 FOREIGN KEY (id_obra) REFERENCES obra(id)
);
