﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.Helpers
{
    public class LogWriter
    {
        public static void writeLog(string message)
        {
            try
            {

                if (!Directory.Exists("./logDir"))
                    Directory.CreateDirectory("./logDir");
                using (var sw = new StreamWriter(@".\logDir\logfile.log", true))
                {
                    sw.WriteLine(DateTime.Now.ToString() +" - "+ message);
                }
            }
            catch
            {

            }
        }
    }
}
