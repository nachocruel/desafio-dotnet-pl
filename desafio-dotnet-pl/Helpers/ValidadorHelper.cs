﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.Helpers
{
    public class ValidadorHelper
    {
        public static bool validarEmail(string email)
        {
            Regex rx = new Regex(@"^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$");
            return rx.IsMatch(email);
        }


        static int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
        static int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
        static string[] invalidosDigitosIguais = {"00000000000", "11111111111", "22222222222", "33333333333",
            "44444444444", "55555555555", "66666666666", "77777777777", "88888888888", "99999999999"};
        public static bool validaCpf(string cpf)
        {
            if (cpf.Length != 11 || invalidosDigitosIguais.Contains(cpf))
                return false;
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }


        public static string[] GetAllCountries()
        {
            try
            {
                using (var sr = new System.IO.StreamReader(@".\Assets\TodosPaises.txt"))
                {
                    string allCountries = sr.ReadToEnd();
                    return allCountries.Split('\n');
                }
            }
            catch (System.IO.FileNotFoundException fne)
            {
                LogWriter.writeLog(fne.Message + " " + fne.StackTrace);
                throw fne;
            }
            catch (DirectoryNotFoundException dnf)
            {
                LogWriter.writeLog(dnf.Message + "" + dnf.StackTrace);
                throw dnf;
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex.Message + " " + ex.StackTrace);
                throw ex;
            }
        }
    }
}
