﻿using System;
using System.Collections.Generic;

namespace desafio_dotnet_pl.Models
{
    public partial class Autor
    {
        public Autor()
        {
            TabAutorObra = new HashSet<TabAutorObra>();
        }

        public long Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public DateTime DataDeNascimento { get; set; }
        public string Pais { get; set; }
        public string Cpf { get; set; }
        public string Sexo { get; set; }

        public virtual ICollection<TabAutorObra> TabAutorObra { get; set; }
    }
}
