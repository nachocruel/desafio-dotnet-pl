﻿using System;
using System.Collections.Generic;

namespace desafio_dotnet_pl.Models
{
    public partial class Obra
    {
        public Obra()
        {
            TabAutorObra = new HashSet<TabAutorObra>();
        }

        public long Id { get; set; }
        public string Nome { get; set; }
        public string LinkImagem { get; set; }
        public DateTime? DataDePublicacao { get; set; }
        public DateTime? DataDeExposicao { get; set; }
        public string Descricao { get; set; }

        public virtual ICollection<TabAutorObra> TabAutorObra { get; set; }
    }
}
