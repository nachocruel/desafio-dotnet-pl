﻿using System;
using System.Collections.Generic;

namespace desafio_dotnet_pl.Models
{
    public partial class TabAutorObra
    {
        public long IdAutor { get; set; }
        public long IdObra { get; set; }

        public virtual Autor IdAutorNavigation { get; set; }
        public virtual Obra IdObraNavigation { get; set; }
    }
}
