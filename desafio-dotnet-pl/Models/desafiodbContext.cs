﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace desafio_dotnet_pl.Models
{
    public partial class desafiodbContext : DbContext
    {
        public desafiodbContext()
        {
        }

        public desafiodbContext(DbContextOptions<desafiodbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Autor> Autor { get; set; }
        public virtual DbSet<Obra> Obra { get; set; }
        public virtual DbSet<TabAutorObra> TabAutorObra { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=127.0.0.1;user id=edvan;password=toor;database=desafiodb", x => x.ServerVersion("8.0.21-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Autor>(entity =>
            {
                entity.ToTable("autor");

                entity.HasIndex(e => e.Cpf)
                    .HasName("cpf")
                    .IsUnique();

                entity.HasIndex(e => e.Email)
                    .HasName("email")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cpf)
                    .HasColumnName("cpf")
                    .HasColumnType("varchar(11)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.DataDeNascimento)
                    .HasColumnName("data_de_nascimento")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Pais)
                    .IsRequired()
                    .HasColumnName("pais")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Sexo)
                    .HasColumnName("sexo")
                    .HasColumnType("varchar(15)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Obra>(entity =>
            {
                entity.ToTable("obra");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DataDeExposicao)
                    .HasColumnName("data_de_exposicao")
                    .HasColumnType("datetime");

                entity.Property(e => e.DataDePublicacao)
                    .HasColumnName("data_de_publicacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasColumnType("varchar(240)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.LinkImagem)
                    .IsRequired()
                    .HasColumnName("link_imagem")
                    .HasColumnType("varchar(320)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(120)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<TabAutorObra>(entity =>
            {
                entity.HasKey(e => new { e.IdAutor, e.IdObra })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("tab_autor_obra");

                entity.HasIndex(e => e.IdObra)
                    .HasName("id_obra");

                entity.Property(e => e.IdAutor).HasColumnName("id_autor");

                entity.Property(e => e.IdObra).HasColumnName("id_obra");

                entity.HasOne(d => d.IdAutorNavigation)
                    .WithMany(p => p.TabAutorObra)
                    .HasForeignKey(d => d.IdAutor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tab_autor_obra_ibfk_1");

                entity.HasOne(d => d.IdObraNavigation)
                    .WithMany(p => p.TabAutorObra)
                    .HasForeignKey(d => d.IdObra)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tab_autor_obra_ibfk_2");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
