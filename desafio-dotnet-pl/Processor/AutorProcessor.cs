﻿using desafio_dotnet_pl.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.Processor
{
    public class AutorProcessor
    {
        private static string[] countries = { "Afghanistan", "Albania", "Algeria", "Andorra", "Angola",
            "Antigua & Deps","Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas",
            "Bahrain", "Bangladesh","Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia",
            "Bosnia Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina", "Burundi", "Cambodia",
            "Cameroon", "Canada", "Cape Verde", "Central ", "African Rep", "Chad", "Chile", "China", "Colombia",
            "Comoros", "Congo", "Congo {Democratic Rep}", "Costa Rica", "Croatia","Cuba", "Cyprus", "Czech Republic",
            "Denmark", "Djibouti", "Dominica", "Dominican","Republic", "East Timor","Ecuador", "Egypt",
            "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland",
            "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea",
            "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia",
            "Iran", "Iraq", "Ireland {Republic}", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan",
            "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea North", "Korea South", "Kosovo", "Kuwait",
            "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya",
            "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi",
            "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania",
            "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia",
            "Montenegro", "Morocco", "Mozambique", "Myanmar, {Burma}", "Namibia", "Nauru",
            "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway",
            "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
            "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russian Federation",
            "Rwanda", "St Kitts & Nevis", "St Lucia", "Saint Vincent & the Grenadines", "Samoa",
            "San Marino", "Sao Tome & Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles",
            "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia",
            "South Africa", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
            "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania",
            "Thailand", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan",
            "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States",
            "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Yemen",
            "Zambia", "Zimbabwe"};


        public static bool cadastrarAutor(Autor autor)
        {

            if (autor == null)
            {
                throw new System.ArgumentNullException("Não foi passado nenhum autor para o cadastro!");
            }

            if (String.IsNullOrEmpty(autor.Cpf) && autor.Pais == "Brazil")
            {
                throw new System.ArgumentException("O CPF é obrigatório para autores brasileiros!");
            }

            if (!String.IsNullOrEmpty(autor.Cpf) && !Helpers.ValidadorHelper.validaCpf(autor.Cpf))
            {
                throw new System.ArgumentException("O CPF informado é inválido!");
            }

            if (!String.IsNullOrEmpty(autor.Email) && !Helpers.ValidadorHelper.validarEmail(autor.Email))
            {
                throw new System.ArgumentException("O email informado é inválido!");
            }

            if (!String.IsNullOrEmpty(autor.Email) && DAO.AutorDAO.getAutorFromEmail(autor.Email) != null)
            {
                throw new System.ArgumentException("Já existe um cadastro para o email informado!");
            }


            if (!String.IsNullOrEmpty(autor.Pais) && !countries.Contains(autor.Pais))
            {
                throw new System.ArgumentException("O país informado não é válido!");
            }


            if (autor.DataDeNascimento == null)
            {
                throw new System.ArgumentException("A data nascimento é um campo obrigatório!");
            }

            if (String.IsNullOrEmpty(autor.Nome))
            {
                throw new System.ArgumentException("O nome do autor é um campo obrigatório!");
            }

            if (!String.IsNullOrEmpty(autor.Cpf) && DAO.AutorDAO.getAutorFormCpf(autor.Cpf) != null)
            {
                throw new System.ArgumentException("Já existe de um autor com o CPF informado!");
            }

            return DAO.AutorDAO.addAutor(autor);
        }

        public static bool deleteAutor(Autor autor)
        {
            try
            {
                if (DAO.ObraDAO.getObrasAutor(autor.Id).Count() > 0)
                {
                    throw new System.ArgumentException("O autor não pode ser removido, porque ele possui obras vinculadas!");
                }

                return DAO.AutorDAO.removeAutor(autor);
            }
            catch (Exception ex)
            {
                Helpers.LogWriter.writeLog(ex.Message);
                throw;
            }
        }
    }
}
