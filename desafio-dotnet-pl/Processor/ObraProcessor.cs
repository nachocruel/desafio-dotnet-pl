﻿using desafio_dotnet_pl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace desafio_dotnet_pl.Processor
{
    public class ObraProcessor
    {

        public static bool cadastrarObra(Obra obra, Autor autor)
        {
            if (String.IsNullOrEmpty(obra.Nome))
            {
                throw new System.ArgumentException("O nome da obra é um campo obrigatório!");
            }

            if (String.IsNullOrEmpty(obra.Descricao))
            {
                throw new System.ArgumentException("A descrição da obra é um campo obrigatório!");
            }

            if (obra.Descricao.Length > 240)
            {
                throw new System.ArgumentException("A descrição da obra não pode conter no máximo 240 caracteres!");
            }

            if (obra.DataDeExposicao != null && obra.DataDePublicacao != null)
            {
                throw new System.ArgumentException("A data de publicação é utilizada mais para livros e demais publicações escritas. \n A data de exposição é utilizada mais para obras que são expostas, como pinturas, esculturas e demais.");
            }

            if (obra.DataDePublicacao == null && obra.DataDeExposicao == null)
            {
                throw new System.ArgumentException("Não foi informado a data de publicação ou a de exposição da obra!");
            }
            return DAO.ObraDAO.addObra(obra, autor);
        }

        public static bool removeObra(Obra obra)
        {
            try
            {
                return DAO.ObraDAO.removeObra(obra);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
