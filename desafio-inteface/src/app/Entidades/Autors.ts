export class Autor {
    id:number
    nome:string
    email:string
    pais:string
    sexo:string
    cpf:string
    dataDeNascimento:Date
    tabAutorObra: []
    
    constructor(autor:any = null) {
        if(autor)
        Object.assign(this, autor)
    }
}