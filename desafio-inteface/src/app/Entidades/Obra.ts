export class Obra {
    Id:number
    Nome:string
    LinkImagem:string
    DataDeExposicao:Date
    Descricao

    constructor(obra:any = null){
        if(obra){
            Object.assign(this, obra)
        }
    }
}
