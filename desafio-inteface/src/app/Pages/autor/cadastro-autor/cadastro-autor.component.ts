import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validator, Validators} from '@angular/forms'
import { HttpClient } from '@angular/common/http'
import { Autor } from 'src/app/Entidades/Autors';

@Component({
  selector: 'app-cadastro-autor',
  templateUrl: './cadastro-autor.component.html',
  styleUrls: ['./cadastro-autor.component.css']
})
export class CadastroAutorComponent implements OnInit {
  
  title:string = "Cadastrar um novo Autor"
  formCadastro:FormGroup
  paises:string [] = []
  formSubimmit:boolean = false
  url:string = "http://localhost:44971/api/autor"
  messageSucesso:string
  constructor(private formBuider:FormBuilder, private http:HttpClient) { }

  countries = [ "Afghanistan", "Albania", "Algeria", "Andorra", "Angola",
            "Antigua & Deps","Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas",
            "Bahrain", "Bangladesh","Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia",
            "Bosnia Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina", "Burundi", "Cambodia",
            "Cameroon", "Canada", "Cape Verde", "Central ", "African Rep", "Chad", "Chile", "China", "Colombia",
            "Comoros", "Congo", "Congo {Democratic Rep}", "Costa Rica", "Croatia","Cuba", "Cyprus", "Czech Republic",
            "Denmark", "Djibouti", "Dominica", "Dominican","Republic", "East Timor","Ecuador", "Egypt",
            "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland",
            "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea",
            "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia",
            "Iran", "Iraq", "Ireland {Republic}", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan",
            "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea North", "Korea South", "Kosovo", "Kuwait",
            "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya",
            "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi",
            "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania",
            "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia",
            "Montenegro", "Morocco", "Mozambique", "Myanmar, {Burma}", "Namibia", "Nauru",
            "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway",
            "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
            "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russian Federation",
            "Rwanda", "St Kitts & Nevis", "St Lucia", "Saint Vincent & the Grenadines", "Samoa",
            "San Marino", "Sao Tome & Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles",
            "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia",
            "South Africa", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
            "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania",
            "Thailand", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan",
            "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States",
            "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Yemen",
            "Zambia", "Zimbabwe" ]

  ngOnInit(): void {
    this.iniciarFormulario()
  }

  iniciarFormulario() {
    this.formCadastro = this.formBuider.group({
      nome: ['', [Validators.required]],
      email: [''],
      cpf: [''],
      pais: ['Brazil', Validators.required],
      dataDeNascimento: ['', Validators.required],
      sexo: ['Masculino']
    })
  }

  validateNome() {
    return this.formSubimmit && this.formCadastro.controls.nome.errors
  }

  validatePais() {
    return this.formSubimmit && this.formCadastro.controls.pais.errors
  }

  validateNascimento() {
    return this.formSubimmit && this.formCadastro.controls.dataDeNascimento.errors
  }

  CadastrarAutor(){
     this.formSubimmit = true
     if(this.formCadastro.invalid) {
       return;
     }

     const autor = new Autor()
     autor.id = 0
     autor.email = this.formCadastro.value.email
     autor.nome = this.formCadastro.value.nome
     autor.dataDeNascimento = new Date(this.formCadastro.value.dataDeNascimento)
     autor.pais = this.formCadastro.value.pais
     autor.sexo = this.formCadastro.value.sexo
     autor.cpf = this.formCadastro.value.cpf
     
     this.http.post(this.url, Object.assign({}, autor)).subscribe((res) => {
       if(res) {
         this.messageSucesso = "Autor Cadastrado com sucesso!"
       }
       setTimeout(() => {
         this.messageSucesso = null
       }, 3000);
     })
  }

}
