import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarAutoresComponent } from './mostrar-autores.component';

describe('MostrarAutoresComponent', () => {
  let component: MostrarAutoresComponent;
  let fixture: ComponentFixture<MostrarAutoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarAutoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarAutoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
