import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/Entidades/Autors';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'

@Component({
  selector: 'app-mostrar-autores',
  templateUrl: './mostrar-autores.component.html',
  styleUrls: ['./mostrar-autores.component.css']
})
export class MostrarAutoresComponent implements OnInit {

  title:string = "Catalogo de Autores"
  autores:Autor [] = []
  url:string = "http://localhost:44971/api/autor"
  messageSucesso:string = null
  messageErro:string = null
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    const header = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');
    const params = new HttpParams().set("min", "0").set("max", "10");

    this.http.get(this.url, {headers: header, params: params}).subscribe((snapShot:[]) => {
       if(snapShot.length) {
         snapShot.forEach(a => {
           this.autores.unshift(new Autor(a))
         })
       }
    })
  }
  
  removerAutor(autor:Autor){
    this.messageErro = null
    this.messageSucesso = null;
    const header = new HttpHeaders({
      'content-type': 'application/json',
    }).set('Access-Control-Allow-Origin', '*')
    this.http.request("delete", this.url, {headers: header, body: Object.assign({}, autor)})
    .subscribe((response) => {
        if(response) {
          this.autores =  this.autores.filter(a => a.id != autor.id)
          this.messageSucesso = "Autor removido com sucesso!"
        } else {
          this.messageErro = "Ocorreu um erro ao tentar remover o autor!"
        }
        
        setTimeout(() => {
          this.messageSucesso = null
          this.messageErro = null
        }, 3000);
    })
  }


  editarAutor(autor:Autor){

  }

  visualizarObras(autor:Autor){

  }
}
