import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { BaseLayoutComponent } from 'src/app/Layout/base-layout/base-layout.component'
import { MostrarAutoresComponent } from 'src/app/Pages/autor/mostrar-autores/mostrar-autores.component'
import {CadastroAutorComponent } from 'src/app/Pages/autor/cadastro-autor/cadastro-autor.component'
import { AtualizaAutorComponent } from './Pages/autor/atualiza-autor/atualiza-autor.component';
import { MostrarObrasComponent } from './Pages/obra/mostrar-obras/mostrar-obras.component';

const routes: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        children: [
            // Autores.
            {
                path: '',
                component: MostrarAutoresComponent,
                data: {extraParameter: 'MenuAutor'}
            },
            {
                path:"autor/cadastro-autor",
                component: CadastroAutorComponent,
                data: {extraParameter: "MenuAutor"}
            },

            {
                path: "autor/atualizar",
                component: AtualizaAutorComponent,
                data: {extraParameter: "MenuAutor"}
            },

            // Obras
            {
                path: 'obra/mostrarObras',
                component: MostrarObrasComponent,
                data: {extraParameter: "MenuObra"}
            }
        ]
        
    },
    {path: '**', redirectTo: ''}
]
@NgModule({
    imports: [RouterModule.forRoot(routes,
      {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled',
      })],
    exports: [RouterModule]
  })
  export class AppRoutingModule {
  }