import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BaseLayoutComponent } from './Layout/base-layout/base-layout.component';
import { MostrarAutoresComponent } from './Pages/autor/mostrar-autores/mostrar-autores.component';
import { CadastroAutorComponent } from './Pages/autor/cadastro-autor/cadastro-autor.component';
import { LayoutAutorComponent } from './Pages/autor/layout-autor/layout-autor.component';
import { AtualizaAutorComponent } from './Pages/autor/atualiza-autor/atualiza-autor.component';
import { MostrarObrasComponent } from './Pages/obra/mostrar-obras/mostrar-obras.component';
import { TitlePageComponent } from './Layout/componets/title-page/title-page.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    BaseLayoutComponent,
    MostrarAutoresComponent,
    CadastroAutorComponent,
    LayoutAutorComponent,
    AtualizaAutorComponent,
    MostrarObrasComponent,
    TitlePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
